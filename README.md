# Intro To Swift for iOS - Part One

## First - Clone this repo and open "Swift Intro One.playground":

```sh
git clone https://bitbucket.org/d2burke/swift-intro-one.git
cd swift-intro-one
open "Swift Intro One.playground"
```

## Second - Swift Programming Basics Using Playgrounds

![Swift Playgrounds](http://i.imgur.com/URG9txm.png "Swift Playgrounds")


```
//Strings
let name:String = "Daniel"

//Simple string concatenation
var fullName = name + " Burke"

//Mutability
//name = "Mr. " + fullName
fullName = "Mr. " + fullName

//Easy variable insertion
var sentence = "My name is \(fullName)"

//No format specifiers required
"This is the \(1)st Swift workshop by \(fullName)"

//Arrays, Mutability
var buttons = [UILabel(), UILabel(),UILabel(), UILabel()]
//let buttons = [UILabel(), UILabel(),UILabel(), UILabel()]

//Index referencing made possible by Type Inferencing!
buttons[1].text = "My Label"

//Array type inference (Optional)
"Label text is: \(buttons[1].text)"

//Familiar methods
buttons.removeAtIndex(1)
"I have \(buttons.count) buttons left"

//Dictionary
var item = Dictionary<String,String>()
item["name"] = "Nerf Gun"
//item["cost"] = 19.99
item["cost"] = "19.99"
item["quantity"] = "32"

//Casting
let cost = Double(item["cost"]!) //Dictionaries allow for non-nil values
let quantity = Int(item["quantity"]!)
let totalValue = cost! * Double(quantity!) //Operations must be between same types
"We have \(item["quantity"]!) \(item["name"]!)s at a \(totalValue) dollar value" //could print a 'nil' here


//Tuples
for (key, value) in item {
    //Tuple Keys
    print("Key: \(key), Value: \(value)")
}

for value in item {
    //Tuple indexes
    print("Key: \(value.0), Value: \(value.1)")
}

//Enums
public enum GradeLevel {
    case Kindergarten
    case FirstGrade
    case SecondGrade
    case ThirdGrade
    case FourthGrade
    case FifthGrade
}

var gradeLevel = GradeLevel.Kindergarten

//Switch Statements
switch gradeLevel {
    case GradeLevel.Kindergarten:
        print("Welcome To Elementary School!")
    case GradeLevel.SecondGrade, GradeLevel.FirstGrade:
        print("Welcome Back!")
    default:
        print("This event is only for Elementary Schoolers")
}

//Type Inference allows abbreviations!
gradeLevel = .SecondGrade


//Switch Statements
switch gradeLevel {
case GradeLevel.Kindergarten:
    print("Welcome To Elementary School!")
case GradeLevel.SecondGrade, GradeLevel.FirstGrade:
    print("Welcome Back!")
default:
    print("This event is only for Elementary Schoolers")
}
```
