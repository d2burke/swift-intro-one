//
//  MovieCell.swift
//  SwiftIntroOne
//
//  Created by Daniel Burke on 1/24/16.
//  Copyright © 2016 D2 Development. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {

    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    
}
