//
//  ViewController.swift
//  SwiftIntroOne
//
//  Created by Daniel Burke on 1/24/16.
//  Copyright © 2016 D2 Development. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    var movies:[NSDictionary] = [
        ["title":"The Hateful Eight","image":"hateful_eight","rating":7.1],
        ["title":"Angry Birds","image":"angry_birds","rating":5.6],
        ["title":"13 Hours","image":"13_hours","rating":8.3],
        ["title":"Star Wars: The Force Awakens","image":"star_wars","rating":8.7],
        ["title":"Deadpool","image":"deadpool","rating":10],
        ["title":"The Finest Hours","image":"finest_hours","rating":6.8],
        ["title":"Kung Fu Panda 3","image":"kung_fu","rating":6.3],
        ["title":"Suicide Squad","image":"suicide_squad","rating":6.9],
        ["title":"The Revenant","image":"the_revenant","rating":8.8]
    ]
    @IBOutlet weak var movieCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UICollectionViewDelegate
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(self.view.frame.size.width*0.48, 300)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let ratingStars = ["*","**","***","****","*****"]
        
        let cell:MovieCell = collectionView.dequeueReusableCellWithReuseIdentifier("movie", forIndexPath: indexPath) as! MovieCell
        let movie = movies[indexPath.row] as NSDictionary
        
        let rating = movie["rating"] as! Double
        let ratingScore = Int(floor(rating/2))
        
        cell.ratingLabel.text = ratingStars[ratingScore-1]
        cell.titleLabel.text = movie["title"] as? String
        cell.posterImageView.image = UIImage(named: movie["image"] as! String)
        return cell
    }
}

